package at.hakwt.swp4.cohesion.traveler.payroll;

import java.time.LocalDate;

public class Expense {

    private double value;

    private LocalDate valueDate;

    public Expense(double value) {
        this.value = value;
        this.valueDate = LocalDate.now();
    }

    public double getValue() {
        return value;
    }
}
