package at.hakwt.swp4.cohesion.traveler.payroll;

public class PayrollData {

    private final Employee employee;

    private final Expense[] expenses;

    private final int noOfOverhours;

    public PayrollData(Employee employee, int noOfOverhours, Expense[] expenses) {
        this.employee = employee;
        this.noOfOverhours = noOfOverhours;
        this.expenses = expenses;
    }

    public Employee getEmployee() {
        return employee;
    }

    public Expense[] getExpenses() {
        return expenses;
    }

    public int getNoOfOverhours() {
        return noOfOverhours;
    }
}
