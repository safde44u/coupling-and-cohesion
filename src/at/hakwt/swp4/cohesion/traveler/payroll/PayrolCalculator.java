package at.hakwt.swp4.cohesion.traveler.payroll;

import java.util.Random;

public class PayrolCalculator {

    public Double getSalary(Employee employee, int noOfOverHours, Expense[] expenses) {
        // get base salary from employee database
        double salary = employee.getSalary();
        // get over-hours to be payed
        double overHoursValue = calculateOverHoursValue(employee, noOfOverHours);
        // get expenses to be refunded
        double totalExpenses = 0d;
        for(Expense e : expenses) {
            totalExpenses += e.getValue();
        }

        return salary + overHoursValue + totalExpenses;
    }

    private double calculateOverHoursValue(Employee employee, int noOfOverHours) {
        // do some heavy calculation
        Random r = new Random();
        return noOfOverHours * r.nextDouble();
    }

    public void print(Employee employee, Double salary, String month) {
        System.out.println(employee.getName() + " bekommt im " + month + " " + Math.round(salary) + " € ausbezahlt");
    }

    public PayrollData importData(String filnName) {
        Expense diaeten = new Expense(24);
        Expense kmGeld = new Expense(42); // 100 km * 0,42 C
        return new PayrollData(new Employee(1500, "Max Mustermann"), 3, new Expense[]{diaeten, kmGeld});
    }
}
