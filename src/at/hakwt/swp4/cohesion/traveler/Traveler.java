package at.hakwt.swp4.cohesion.traveler;

import at.hakwt.swp4.cohesion.traveler.payroll.*;

public class Traveler {


   private Garage garage;
   private Car car;
   private Restaurant restaurant;
   private Chef chef;
   private Meal meal;
    public void startJourney() {
        // ...
    }

    public void maintainCar() {
    garage.maintainCar(car);
    }

    public void cook() {
      Meal meal = restaurant.cook(chef);
    }

}
