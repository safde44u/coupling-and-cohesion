package at.hakwt.swp4.coupling.company;

public class Manager {

    private Worker[] workersToManage;

    public Manager(Worker[] workers) {
        this.workersToManage = workers;
    }

    public void manageWork() {
        for (Worker w : workersToManage) {
            w.process(new String[]{"this", "and", "that"});
        }
    }

}
