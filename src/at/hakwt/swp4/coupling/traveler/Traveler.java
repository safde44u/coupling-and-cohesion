package at.hakwt.swp4.coupling.traveler;

public class Traveler {

    private Car car;

    public Traveler() {
        this.car = new Car();
    }

    public void startJourney() {
        this.car.move();
    }

}
