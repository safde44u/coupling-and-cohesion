public class Motorcycle extends Vehicle
{

    @Override
    public void move() {
        System.out.println("Motorcycle is moving");
    }
}
