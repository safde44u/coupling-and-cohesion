public class TravelerMain
{
    public static void main(String[] args) {
        Traveler t = new Traveler(new Car());
        t.startJourney();
        Traveler motorcycleTraveler = new Traveler(new Motorcycle());
        motorcycleTraveler.startJourney();
    }
}
